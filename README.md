# README #



### What is this repository for? ###

We may want to render a component with more than one look and feel, 
but not want to mix the HTML in one file. For example, one version of the component is plain, 
and another version displays an image and extra text. In this case, we can import multiple HTML 
templates and write business logic that renders them conditionally.

### Technical Details ###
Create multiple HTML files in the component bundle. 
Import them all and add a condition in the render() method to return the correct template depending on the componentís state. 
The returned value from the render() method must be a template reference, which is the imported default export from an HTML file.

### Functional Details:  ###
1. The main advantage of using two html templates is that we can use the same JS file and the controllers 
but still display the data in different style formats.
2. We can also keep different css files for both HTML Templates.
3. This functionality can also be used in a case when we want to use 2 completely different 
front end designs for mobile and desktop users but want to show the same data. Media query works only if we 
have a similar kind of design which is to be converted to desktop/mobile friendly environment but render() allows for completely different look and feel.

### Example:  ###
1. import { LightningElement } from "lwc";
2. import smallScreen from "./smallScreen.html";
3. import largeScreen from "./largeScreen.html";
 
4. export default class RenderHtml extends LightningElement {
5.   render() {
6.     return window.screen.width < 768 ? smallScreen : largeScreen;
7.     // Here smallScreen and largeScreen are the references for the imported HTML      templates and they are being rendered depending on the device width.
8.   }
9. }



